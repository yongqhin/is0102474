<?php

namespace App\Http\Controllers;


class IS0102474 extends Controller
{
  public function index()
    {
        $is102474=\App\is0102474::all();
        return view('index',compact('is0102474'));
    }

public function create()
    {
        return view('create');
    }

   
public function store()
    {
        $is0102474 = new \App\is0102474();
        $is0102474->year=request('year');
        $is0102474->semester=request('semester');
        $is0102474->CGPA=request('CGPA');
        $is0102474->save();
        return redirect('is0102474')->with('success', 'Information saved');
    }  
}

