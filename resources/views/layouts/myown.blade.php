<!DOCTYPE html>
<html lang="en">
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CSEB574 - Model &amp; Relationship</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
 </head>
    <body>
        <div class="container">
            @yield('content')
    </div>
    <script src="{{ asset('js/app.js') }}" type="text/js"></script>
    </body>
</html>