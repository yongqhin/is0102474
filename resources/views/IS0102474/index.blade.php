@extends('layouts.myown')

@section('content')

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <h1 class="display-3">Department List</h1>
        <a href="{{ route('is0102474.create') }}" class="btn btn-primary floatright">Create</a>
 
        <table class="table">
            <tr>
             <th>No.</th><th>Name</th><th>Action</th></tr>
            </tr>

                @foreach($is0102474 as $dept)

                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $dept->name }}</td>
                        <td>

                            <a href="{{ route('is0102374.index',$dept->id)}}">index</a> &nbsp;
                            <a href="">Delete</a>

                        </td>
                    </tr>
                @endforeach
                
        </table>
        </div>
    </div>
@endsection